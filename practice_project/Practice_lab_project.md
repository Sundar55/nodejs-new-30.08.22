<center>
<img src="images/IDSN-new-logo.png" width = "300">
</center>

# Practice Project - Create a CRUD Rest API

## Estimated Effort: 1 Hr 30 mins

## Objectives: 

- Create API endpoints to perform Create, Retrieve, Update and Delete operations on transient data with express server.

- Access CRUD endpoints and test CRUD operation with Postman.

<br><br>

## Set-up : Create application
1. Open a terminal window by using the menu in the editor: Terminal > New Terminal.

<img src="images/new-terminal.jpg" width="75%"/>


2. Change to your project folder, if you are not in the project folder already.

```
cd /home/project
```

{: codeblock}


3. Run the following command to clone the git repository that contains the starter code needed for this lab, if it doesn't already exist.

```
[ ! -d 'Practice-lab' ] && git clone https://github.com/sapthashree15/Practice-lab.git
```
{: codeblock}

<img src="Images-practice/git-clone.jpg" width="75%"/>

5. Change to the directory **Practice-lab** directory to start working on the lab.

```
cd Practice-lab/
```

{: codeblock}

<img src="Images-practice/cd_to_correct_folder.jpg" width="75%"/>

6.  List the contents of this directory to see the artifacts for this lab.

```
ls
```
{: codeblock}

<img src="Images-practice/ls_command.jpg" width="75%"/>

#

## Exercise 1: Accessing the basic Express app:

1.  In the files explorer open the **Nodeexe** folder and view **index.js**.

<img src="images/ex1-index.js.png" width="75%"/>

It will have the following content:

```
const express = require('express');

const routes = require('./routes/users.js');


const app = express();
const PORT =5000;


app.use("/user", routes);

app.listen(PORT,()=>console.log("Server is running"));
```

This basically means that your express server has been configured to run at port 5000. When you access the server with **/user** you can access the end points defined in **routes/users.js**. You have to install two packages, namely express and nodemon, before you can run your server.

2. In the terminal window, run the following command to install the **express** package.

```
npm install express --save
```
{: codeblock}

<img src="images/ex1-express installation.png" width="75%"/>

3. Run the following command to install the **nodemon** package is installed. 

```
npm install nodemon --save
```
{: codeblock}

<img src="images/ex1-nodemon installation.png" width="75%"/>

<br><br>

#

## Exercise 2: Develop your Express app:

1. Navigate to the file named `users.js` in the `routes` folder. It has some starter code provided already as seen in the image below.

<img src="images/ex2-users.js.png" width="75%"/>

<br><br>

2. **R** in CRUD stands for retrieve. You will first add a GET API endpoint, using the **get** method for getting the details of all users. A few users have been added in the starter code. Copy the code below and paste in users.js.

```
router.get("/",(req,res)=>{
    res.send(users);
});
```
{: codeblock}

<img src="images/Ex3-GET method.png" width="75%"/>

4. To test this endpoint, in the terminal window run the server with the following command:

```
npm start 
```

<img src="images/Ex3-npm start.png" width="75%"/>


5. Click on the Skills Network button on the right, it will open the "Skills Network Toolbox". Then click `OTHER` then `Launch Application`. From there you should be able to enter the port as `5000` and launch the development server.

<img src="images/Launch_Application.png" width="80%" style="border: solid 1px grey"/>

6. When the browser page opens up, suffix **/user** to the end of the URL on the address bar.You will see the below page.

<img src="images/ex2-GET-output.png" width="75%"/>

7. We can check the output of the GET request using curl command 

`curl localhost:5000/user/curl localhost:5000/user/`


<br><br>

## Exercise 3: Creating a GET by specific ID method:

1. Create a GET method for getting the details of a specific user based on his/her email ID by using the `filter` method to filter a specific user by `email`.


<details><summary>You can also click here to view the code</summary>

```
router.get("/:email",(req,res)=>{
    const email = req.params.email;
    let filtered_users = users.filter((user) => user.email === email);
    res.send(filtered_users);
}) 
```

<img src="images/Ex3-GET by specific ID.png" width="75%"/>

</details>


2. In the terminal window run the server with the following command:

```
npm start 
```

<img src="images/Ex3-npm start.png" width="75%"/>


3. Click on Terminal > Split Terminal

<img src="images/ex2-split terminal.png" width="75%"/>

Open a new terminal & use the below command to view the output for the user with mail id 'curl localhost:5000/user/johnsmith@gamil.com':

```
curl localhost:5000/user/curl localhost:5000/user/johnsmith@gamil.com
```

<img src="images/Ex3-GET by specific ID-CURL.png" width="75%"/>


## Exercise 4: Creating the POST method:

1. The **/user** endpoint with POST method is used for adding a user to the list. We send the user details as query parameters with the `push` function for creating a POST method for adding a new user with the details - `firstName`, `lastName`, `DOB` and `email` & pushing it back to the `users` array

```
router.post("/new/",(req,res)=>{
    users.push({"firstName":req.query.firstName,"lastName":req.query.lastName,"email":req.query.email,"DOB":req.query.DOB});
    res.send("The user" + (' ')+ (req.query.firstName) + " Has been added!")
}); 
```

2. The completed code will look like this.

<img src="images/Ex4-POST.png" width="75%"/>

3. Use the below command to post a new user with mail id 'alansmith@gamil.com' on the split terminal:

```
curl --location --request POST 'localhost:5000/user/new?firstName=Alan&lastName=Smith&DOB=21/07/1975&email=alansmith@gamil.com' \
--header 'Cookie: jhub-reverse-tool-proxy=s%3A236aaba1-d6d5-4fdc-935d-16a5d8d2dbdf.xbGQzfpVVwrdlV0jAN%2B3hroqp6GRMWd%2BK9ocbgOaHSA'
```

4. The ouput will be as below:

<img src="images/Ex4-POST-CURL.png" width="75%"/>

<br><br>

5. To verify if the user with email 'alansmith@gamil.com' has been added, you can send a GET request as below:

```
curl localhost:5000/user/alansmith@gamil.com
```

<img src="images/Ex4-GET request-after-POST.png" width="75%"/>

## Exercise 5: Creating the PUT method:

1. The **U** in CRUD stands for update which can be achieved using the PUT method. To make updates in the data, we use PUT method. We first look for the object based on the email and then update one specific details about that users. 

```
router.put("/:email", (req, res) => {
    const email = req.params.email;
    let filtered_users = users.filter((user) => user.email === email);
    if (filtered_users.length > 0) {
        let filtered_user = filtered_users[0];
        let DOB = req.query.DOB;
        //Create a query object for `firstName` similar to `DOB`
        //Create a query object for `firstName` similar to `DOB`
        if(DOB) {
            filtered_user.DOB = DOB
        }
        //Create an `if` condition for `firstName` similar to `DOB`
        //Create an `if` condition for `lastName` similar to `DOB`
        users = users.filter((user) => user.email != email);
        users.push(filtered_user);
        res.send(`User with the email  ${email} updated.`);
    }
    else{
        res.send("Unable to find user!");
    }
  });
  ```
2. The completed code will look like this.

<img src="images/Ex4-PUT full code.png" width="75%"/>

3. Use the below command to update the `DOB` as `1/1/1971` for the user with mail id 'johnsmith@gamil.com' in the split terminal:

```
curl --location --request PUT 'localhost:5000/user/johnsmith@gamil.com?DOB=1/1/1971' \
--header 'Cookie: jhub-reverse-tool-proxy=s%3A236aaba1-d6d5-4fdc-935d-16a5d8d2dbdf.xbGQzfpVVwrdlV0jAN%2B3hroqp6GRMWd%2BK9ocbgOaHSA'
```

4. The ouput will be as below:

<img src="images/ex5-PUT-CURL.png" width="75%"/>

<br><br>

5. To verify if the `DOB' of the user with email 'johnsmith@gamil.com' has been updated, you can send a GET request as below:

```
curl localhost:5000/user/johnsmith@gamil.com
```

<img src="images/ex5-GET request-after-PUT.png" width="75%"/>

## Exercise 6: Creating the DELETE method:

1. Create a DELETE method for deleting specific users by email by using the below code:

```
router.delete("/:email", (req, res) => {
    const email = req.params.email;
    users = users.filter((user) => user.email != email);
    res.send(`User with the email  ${email} deleted.`);
  });
```
2. The completed code will look like this.
<img src="images/Ex6-DELETE full code.png" width="75%"/>

3. Use the below command to delete the user with mail id 'johnsmith@gamil.com' in the split terminal:

```
curl --location --request DELETE 'localhost:5000/user/johnsmith@gamil.com' \
--header 'Cookie: jhub-reverse-tool-proxy=s%3A236aaba1-d6d5-4fdc-935d-16a5d8d2dbdf.xbGQzfpVVwrdlV0jAN%2B3hroqp6GRMWd%2BK9ocbgOaHSA'
```

4. The ouput will be as below:

<img src="images/ex6-DELETE-CURL.png" width="75%"/>

5. Send a GET request for the user with email 'johnsmith@gamil.com' and ensure that a null object is returned:

<img src="images/ex6-_GET_request_after_DELETE.png" width="75%"/>

<br><br>

## Exercise 7: Testing the output of the above methods:

- Please update the code for the GET method to:

```
router.get("/",(req,res)=>{
    res.send(users);
    console.log(JSON.stringify(users));
});
```

- The link that you will get after launching your app from Theia on port 5000 will be: "https://<your SN labs username>-5000.theiadocker-3-labs-prod-theiak8s-4-tor01.proxy.cognitiveclass.ai/"


- The link for GET reqeust for all users will be: "https://<your SN labs username>-5000.theiadocker-3-labs-prod-theiak8s-4-tor01.proxy.cognitiveclass.ai/user"

If you click on this link, you will see the JSON equivalent of the output on your terminal based on the updated GET method.



We have tested the GET methods of our API in our web browser and seen responses. But we can’t test POST, PUT and DELETE HTTP methods in a web browser. To test those methods, we use Postman. 

> Note: You can refer to <a href = "https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-CD0201EN-SkillsNetwork/labs/5_RestArchitecture/Instructions_CloudantWithRESTAPI.md.html?origin=www.coursera.org"> this </a> lab to get familiar with and understand how to send requests through Postman.

Go to <a href="https://web.postman.co">Postman</a> and create a new login or sign in with your Google mail credentials login. 

> Note: Make sure that your server is running and the Server is listening on port 5000. 

Let's start testing by first making an HTTP GET Request. 
 
1. <b>GET request</b>

a. Enter the GET request URL  ( https://XXXXXXXXXX-5000.theiadocker-0-labs-prod-theiak8s-4-tor01.proxy.cognitiveclass.ai/user) in your input box of Postman where you see “Enter Request URL”. 

<img src="images/ex7-GET request-URL entry.png" width="75%"/>

b. Click on the “Send” button after entering the URL. 
<img src="images/ex7-GET request-send.png" width="75%"/>

c. The output will be as below:

<img src="images/ex7-GET request-output.png" width="75%"/>
<br><br>

2. <b>GET request by specific ID</b>

a. Enter the request URL by adding the specific email address to the above GET 	request URL. If the email address is johnsmith@gmail.com then enter the below URL in the input box of postman. ( https://XXXXXXXXXX-5000.theiadocker-0-labs-prod-theiak8s-4-tor01.proxy.cognitiveclass.ai/user/johnsmith@gmail.com) 

<img src="images/ex7-GET-byID-URL entry.png" width="75%"/>

b. Click on the “Send” button after entering the URL to view the output. 
<img src="images/ex7-GET-byID-send.png" width="75%"/>

c. The output will be as below:

<img src="images/ex7-GET request-output.png" width="75%"/>

<br><br>

3. <b>POST request : </b>

a. Enter the basic post request URL: (https://XXXXXXXXXX-5000.theiadocker-0-labs-prod-theiak8s-4-tor01.proxy.cognitiveclass.ai/user/new) . Ensure to select the POST method and select the “Params”. 

<img src="images/ex7-POST-base-URL.png" width="75%"/>

b. Enter the firstName as 'Bob',lastName as 'Smith' ,email as 'bobsmith@gamil.com` and DOB as '1/1/1978' for a new user:

<img src="images/ex7-POST_key-value.png" width="75%"/>

D. Click on the “Send” button after entering the URL to view the output. 

<img src="images/ex7-POST_send.png" width="75%"/>

Verify that the newly added values are been updated by doing the GET request.
> Note: Ensure that you delete any parameters that you added for the POST request before sending the GET request. 

<img src="images/ex7-GET_after-POST.png" width="75%"/>

<br><br>

4. <b>PUT request: </b>

a. Enter the URL by adding the specific email address. If the email address is bobsmith@gmail.com then enter the below URL in the input box of the Postman ( https://XXXXXXXXXX-5000.theiadocker-0-labs-prod-theiak8s-4-tor01.proxy.cognitiveclass.ai/user/bobsmith@gmail.com).  Ensure to select the PUT method and select the “Params”. 

<img src="images/ex7-PUT-base-URL.png" width="75%"/>

b.Enter the key and values to be changed. For example, if you want to change the “DOB” key and replace it with the new value 1/1/1981 it will be as below. 

<img src="images/ex7-PUT_key-value.png" width="75%"/>

d.  Click on the “Send” button after entering the URL to view the output. 

<img src="images/ex7-PUT_send.png" width="75%"/>


Verify that the newly added values are been updated by doing a GET request.

> Note: Ensure that you delete any parameters that you added for the PUT request before sending the GET request. 

<img src="images/ex7-GET_after-PUT.png" width="75%"/>

<br><br>

5. <b>DELETE Request: </b>

 a. Enter the URL by adding the specific email address. If the email address is bobsmith@gmail.com then enter the below URL in the input box of the Postman. ( https://XXXXXXXXXX-5000.theiadocker-0-labs-prod-theiak8s-4-tor01.proxy.cognitiveclass.ai/user/bobsmith@gmail.com). Ensure to select the DELETE method. 

<img src="images/ex7-DELETE-base-URL.png" width="75%"/>

c.  Click on the “Send” button after entering the URL to view the output. 

<img src="images/ex7-DELETE-send.png" width="75%"/>

Verify that the GET user by ID `bobsmith@gmail.com` returns a null object by sending a GET request.

> Note: Ensure that you delete any parameters (if any are there) before sending the GET request. 

<img src="images/ex7-GET-after-DELETE.png" width="75%"/>

<br><br>

### Congratulations! You have completed the lab for CRUD operations with Node.js and Express.js using Postman. 

## Summary: 

In this lab, we have performed CRUD Operations like GET, POST, PUT and DELETE on an Express App and tested the above methods using Postman. 


## Author(s)
<h4> Sapthashree K S <h4/>
<h4> K Sundararajan <h4/>


## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 30-08-2022 | 1.0  | K Sundararajan | Initial version created |
| 01-09-2022 | 1.1  | K Sundararajan | Updated instructions |
| 02-09-2022 | 1.2  | Sapthashree K S | Updated instructions |
| 02-09-2022 | 1.3  | K Sundararajan | Updated instructions |


## <h3 align="center"> (C) IBM Corporation 2020. All rights reserved. <h3/>
